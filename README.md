# Retrieval Information

en este repositorio se encuentra alojado la actividad solicitada segun las especificaciones de https://ccc.inaoep.mx/~mmontesg/pmwiki.php/Main/RecuperacionDeInformacion


La actividad fue realizada en Python 3.8.10

Requisitos minimos:
 
- tener instalado python 3.8 o mayor

- Sistema Operativo Linux - Windows 

- Computador Intel Dual Core 2.0 o Mayor

Proceso de Instalacion.

- Instalar Python 3.8 o mayor

- Instalar la Libreria Stemmer 2.0.1 ejecutando el comando 

- **_Python pip Stemmer==2.0.1_**


Proceso de ejecucion.

para ejecutar este programa se debe tener abierto el shell de comandos y ejecutar

_python main.py > results/test.out_

en el se va a generar 3 archivos con extensiones .out el cual contendran los datos de resultado como se muestran a continuacion.

en el archivo **_test.out_** se podran encontrar datos estadisticos como de cuantos documentos y Consultas han sido recuperados, el promedio de palabras truncadas por documento, el promedio de palabras vacias por documento, entre otros, encontrandose estructurado de la siguiente forma:


Retrieval Information beginning.......

N° Documentos Recuperados: 423

N° Consultas Recuperadas: 83

N° de Palabras Vacias: 340

    Documento N° 1  Promedio de Sustitucion de Palabras Vacias: 0.2426252400069820

                    Promedio de Truncado con Porter: 0.013964042590329906

                    Promedio de Terminos de Frecuencia: 419
    .
    .
    .

    Consulta N° 1 Promedio de Sustitucion de Palabras Vacias: 0.11764705882352944

                  Promedio de Truncado con Porter: 0.0117647058823529

                  Promedio de Terminos de Frecuencia: 12


mientras que en los otros archivos **DOCS-tf y QUERYS-tf**__

siguen el patron expuesto en la actividad el cual es:

D_oc1 termino1-1 frecuencia1-1 termino1-2 frecuencia1-2 …

Doci terminoi-1 frecuenciai-1 terminoi-2 frecuenciai-2 …

Y de las consultas:

Query1 termino1-1 frecuencia1-1 termino1-2 frecuencia1-2 …

Queryj terminoj-1 frecuenciaj-1 terminoj-2 frecuenciaj-2 …_



