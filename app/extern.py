"""
Set of constants allowed along the proyect scope.
"""

class Colections:
    DOCS = 'ext/time/TIME.ALL'
    QUERY = 'ext/time/TIME.QUE'
    EMPTY_WORDS = 'ext/time/TIME.STP'
