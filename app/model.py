"""""
    Autor: Henry J Fontalba L.
    Objetivo: Este script se encarga de gestionar
    todos los procesos y mostrar los resultados 
    de los documentos recuperados.
    Fecha: 25 - 05 - 2021
"""

from app.controller import Controller


class Models:

    @staticmethod
    def startRetrievalProcess():
        print('Retrieval Information beginning.......\n')

        docs = Controller.getDocument()
      
        print(f'-----------------------------------------')
        
        docs_Structured = Controller.composeText(docs)
        print(f'N° Documentos Recuperados: {len(docs_Structured)}')
        

        queries_docs = Controller.getQuery()
     
        print(f'-----------------------------------------')
        
        queries_structured = Controller.composeQuery(queries_docs)
        print(f'N° Consultas Recuperadas: {len(queries_structured)}')
        print(f'-----------------------------------------')
        
        emptyWords = Controller.getEmptyWords()
        print(f'N° de Palabras Vacias: {len(emptyWords)}\n')
        print(f'------------------------------------------------\n')


        # procesando todos los documentos de textos.
        i = 1
        for doc in docs_Structured:
            # Quitando las palabras vacias segun la colecion
            doc['cleanBody'] = Controller.replace_String(emptyWords, doc['body'], ' ')
            print(f'Documento N° {i}  Promedio de Sustitucion de Palabras Vacias: { 1 - (len(doc["cleanBody"]) / len(doc["body"])) }')

            # Truncando el Texto mediante el Algoritmo de Porter   
            doc['cleanBodyPorter'] = Controller.truncateStringWithPorter(doc['body'])
            print(f'                 Promedio de Truncado con Porter: { 1 - (len(doc["cleanBodyPorter"]) / len(doc["body"])) }')

            # Obteniendo las relevacias de los documentos (Tf)
            doc['termFrecuency'] = Controller.getTermFrecuency(doc['body'])
            print(f'                 Promedio de Terminos de Frecuencia: { len(doc["termFrecuency"].keys()) }\n')
            i += 1


        # Procesando todas los documentos de Consutlas
        i = 1
        for query in queries_structured:
            
            # Quitando las palabras vacias segun la colecion
            query['cleanBody'] = Controller.replace_String(emptyWords, query['body'], ' ')
            print(f'Consulta N° {i} Promedio de Sustitucion de Palabras Vacias: { 1 - (len(query["cleanBody"]) / len(query["body"])) }')

            # Truncando el Texto mediante el Algoritmo de Porter   
            query['cleanBodyPorter'] = Controller.truncateStringWithPorter(query['body'])
            print(f'                Promedio de Truncado con Porter: { 1 - (len(query["cleanBodyPorter"]) / len(query["body"])) }')

            # Obteniendo las relevacias de los documentos (Tf)
            query['termFrecuency'] = Controller.getTermFrecuency(query['body'])
            print(f'               Promedio de Terminos de Frecuencia: { len(query["termFrecuency"].keys()) }\n')
            i += 1

        Controller.saveTermFrecuency(docs_Structured,'DOCS-tf.out', 'text')
        Controller.saveTermFrecuency(queries_structured, 'QUERYS-tf.out', 'query')
        pass