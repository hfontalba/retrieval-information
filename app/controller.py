"""""
    Autor: Henry J Fontalba L.
    Objetivo: Este script se encarga de gestionar
    y procesar todos los documentos y consultas 
    aplicando los metodos de recuperacion para 
    lenguaje natural.
    Fecha: 25 - 05 - 2021
"""


#Importo las Librerias que necesito.

from app.extern import Colections
import re
import Stemmer


class Controller:

    """
      Metodo openFile: Permite aperturar un archivo a procesar
      Parametros: string que representa al nombre del archivo.
      Retorna: String con los datos obtenido del archivo.
    """
    @staticmethod
    def openFile(inputFile:str) -> str:
        data = ''
        try:
            with open(inputFile, 'r') as f: 
                data = f.read() 
                f.close()
        except Exception as e:
            print(f'Error {str(e)}: Archivo no Encontrado y/o No existe')

        return data

    """
        Metodo getDocument: Permite Obtener los datos de un documento
        Parametros: 
        Retorna:lista de documentos.
    """

    @staticmethod
    def getDocument() -> list:
        data = Controller.openFile(Colections.DOCS)
        docs = re.findall(r'(\*TEXT [^*]+)', data)

        return docs

    """
        Metodo getQuery: Permite Obtener los datos de una consulta
        Parametros: 
        Retorna:lista de consultas.
    """

    @staticmethod
    def getQuery() -> list:
        data = Controller.openFile(Colections.QUERY)
        docs = re.findall(r'(\*FIND [^*]+)', data)

        return docs

    """
        Metodo getEmptWord: Permite Obtener las palabras vacias de un documento
        Parametros: 
        Retorna:lista de palabras Vacias.
    """    

    @staticmethod
    def getEmptyWords() -> list:
        data = Controller.openFile(Colections.EMPTY_WORDS)
        return re.findall(r'\w+', data)


    """
        Metodo composeText: Este metodo se encarga de componer una 
        estructura de dato que permita utilizar textos en un sentido
        Parametros: Lista de documentos
        Retorna:lista de Documentos a Procesar.
    """     

    @staticmethod
    def composeText(docs:list) -> list:
       
        #Lista de Textos para procesar
        Texts = []

        #Armo todo los documentos
        for doc in docs:

            #Estructura que me compone el documento a procesar
            templateText = {}

            #Cabezera del documento.
            header = re.findall(r'(\*TEXT \d{3}\s+(\d+/\d+/? ?\d+)+\s+PAGE\s+\d+\s+)', doc) 
            
            #Cuerpo del documento.
            body = re.findall(r'.*\s\s((.+\s+)+).*\s\s', doc) 

            
            #verifico que el documento tenga un corpus o Body
            if len(body):
                if type(body[0]) == tuple:
                    if len(body[0]):
                        templateText = {'body' : body[0][0] }

                #verifico que ya se haya creado el corpus del documento
                if templateText != {} and len(header):
                    if type(header[0]) == tuple:
                        header = header[0][0].split(' ')
                        templateText.update({
                            'text': header[1],
                            'date': header[2],
                            'page': header[4].replace('\n\n', '')
                        })

            #verifico la cantidad de elementos que tiene el documento
            if len( list(templateText.keys()) ) == 4:
                Texts.append(templateText)
            else:
                print(f'header: {header}')
                print(f'doc: {doc[:32]}\n\n')


        #retorno el documento estructurado.
        return Texts

    
    """
        Metodo composeQuery: Este metodo se encarga de componer una 
        estructura de dato que permita utilizar las consultas para
        un documento.
        Parametros: Lista de Consultas
        Retorna:lista de Consultas a Procesar.
    """ 

    @staticmethod
    def composeQuery(queries:list) -> list:
        
        #Declaro un arreglo de consultas a procesar
        qry = []

        #Obtengo todas las consultas
        for query in queries:
            
            #Estructura para armar una consulta
            templateQuery = {}

            #obtengo la lista de cabezeras de las consultas
            header = re.findall(r'(\*FIND\s+(\d+)\s+)', query) 

            #obtengo el cuerpo de las consultas
            body = re.findall(r'.*\s\s((.+\s+)+).*\s\s', query) 

            #Verifico que las consultas tenga un cuerpo estructurado
            if len(body):
                if type(body[0]) == tuple:
                    templateQuery.update({
                        'body': str(body[0][0])
                    })

                #verifico que se haya guardado el corpus de la consulta
                if templateQuery != {} and header:
                    if type(header[0]) == tuple:
                        templateQuery.update({
                            'query': int(header[0][1]) 
                        })

            
            #verifico la cantidad de elementos que tiene la consulta sea la correcta
            if len( list(templateQuery.keys()) ) == 2:
                qry.append(templateQuery)
            else:
                print(f'header: {header}')
                print(f'query: {query[:32]}\n\n')


        #retorno la Consulta Estructurada.
        return qry


    """
        Metodo replace_String: Este metodo se encarga de reemplazar 
        las palabras que sean iguales en cada documento..
        Parametros: string patter, string, repl
        Retorna:string | lista de string reemplazadas.
    """ 

    @staticmethod
    def replace_String(pattern, string:str, repl:str='') -> str:
        
        #Verifico si es un String o una Lista de Strings
        if type(pattern) == list:
            #Si es una Lista Busco todas los Strings iguales
            pattern = [f'\s+{str(word)}\s+|' for word in pattern]
            pattern = ''.join(pattern)[:-1]

        #Si no es un string es por que es cualquier otro dato
        elif type(pattern) != str:
            print('Error El tipo de Dato de "pattern" debe ser string o lista de strings.')
            return ''

        #Retorno el estring reemplazado.
        return re.sub(pattern, repl, string)



    """
        Metodo truncateStringWithPorter: Este metodo se encarga de
        aplicar el algoritmo de porter a una consulta o a un documento
        haciendo uso de la libreria Stemmer
        Parametros: string data
        Retorna:String.
    """     


    @staticmethod
    def truncateStringWithPorter(data:str) -> str:

        #inicializo la clase Stemming para usar el algoritmo de porter
        stemmer = Stemmer.Stemmer('porter')

        #obtengo e inicializo el splitData para realizar los cambios de string
        splitData = re.findall(r'\w+', data)
        splitData = list(set(splitData))

        #aplico el stemming de porter y trunco el string de cambios.
        porterTruncate = stemmer.stemWords(splitData)

        #verifico que el algoritmo de porter se haya ejecutado bien
        if len(splitData) != len(porterTruncate):
            print('[ERROR] Length of splitData and porterTruncate are diff.\n')
            return data

        word_out = str(data)
        n = len(porterTruncate) #n = la cantidad de palabras truncadas.

        #aplico el truncamiento de todas los documentos de Textos | Consultas
        for i in range(0, n):
            pattern, replace = f'\s+{splitData[i]}\s+', f' {porterTruncate[i]} '

            # Applying the truncate (reduction)
            word_out = re.sub(pattern, replace, word_out)
        
        #retorno el string ya truncado.
        return word_out


    """
        Metodo getTermFrecuency: Este metodo se encarga de
        obtener la frecuencia de uso de las palabras en el
        documento | consulta, para asi saber su relevancia
        Parametros: string data
        Retorna:diccionario con la cantidad de terminos repetidos.
    """         

    @staticmethod
    def getTermFrecuency(data:str) -> dict:
        
        #Inicializo el vector de frecuencias
        frecuency = {}

        #obtengo las palabras | consultas de los documentos
        splitData = re.findall(r'\w+', data)
        splitData = list(set(splitData))

        #Obtengo la frecuencia y las contabilizo
        for word in splitData:
            frecuency[word] = len(re.findall(word, data))

        #Retorno el vector de frecuencias.
        return frecuency


    """
        Metodo setTermFrecuency: Este metodo se encarga de
        guardar la frecuencia de uso de las palabras en el
        documento | consulta, en un archivo de salida segun lo
        indicado en los topicos solicitados.
        Parametros: data: list, FileName:string, key:string
    """ 


    @staticmethod
    def saveTermFrecuency(data:list, FileName:str, possitionKey:str):
       
        #genero la ruta donde se va almacenar el archivo de salida.
        FileName = f'results/{FileName}'

        #verifico el prefijo del Archivo de salida (Query | Docs)
        preffix = 'Query' if possitionKey == 'query' else 'Doc'

        #aperturo el archivo de salida y Estructuro los Resultados
        try:
            with open(FileName, 'w') as f:
                for text in data:

                    #Estructuro el Archivo segun los solicitado: 
                    #Doc1 termino1-1 frecuencia1-1 termino1-2 frecuencia1-2 … | Query1 termino1-1 frecuencia1-1 termino1-2 frecuencia1-2 …
                    
                    line = f'{preffix} {text[possitionKey]}' 
                    for key, value in text['termFrecuency'].items():
                        line += (f' {key} {value}')

                    #escribo los resultados en el archivo.
                    f.write(f'{line}\n\n')

                #Cierro el archivo.
                f.close()

        #Si no se pudo aperturar el archivo y/o no existe la ruta se muestra el error de excepcion.
        except Exception as e:
            print(f'ERROR!!! Al momento de crear el archivo "{FileName}" Verificar la Excepcion: {str(e)}')
            return 

