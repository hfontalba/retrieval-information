"""""
    Autor: Henry J Fontalba L.
    Objetivo: Tarea 2: recuperacion de informacion
    Con la colección "Time", consistente de 423 documentos con 83 
    consultas asociadas con sus correspondientes juicios de relevancia, 
    hacer lo siguiente: 
    1)Preprocesar cada documento con: 
    a) Eliminación de palabras vacías, usando la lista que viene asociada a la colección. 
    b) Truncamiento con el algoritmo de Porter.
    2) Extraer el vocabulario de cada documento junto con su frecuencia de término (tf).
    
    Referencia: https://ccc.inaoep.mx/~mmontesg/pmwiki.php/Main/RecuperacionDeInformacion

    Fecha: 25 - 05 - 2021
"""

# Importando Liberias.


import time

from app.model import Models


if __name__ == '__main__':
    past = int(time.time())

    Models.startRetrievalProcess()

    now = int(time.time())

    print(f'Retrieval Information Process has End. Time elapsed: {now - past} second(s)')
